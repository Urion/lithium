package lithium

import lithium.Lithium.click
import lithium.Lithium.el
import lithium.Lithium.exists
import lithium.Lithium.getDefaultTimeout
import lithium.Lithium.getRightBy
import lithium.Lithium.getTitle
import lithium.Lithium.goTo
import lithium.Lithium.isClickable
import lithium.Lithium.isPageLoaded
import lithium.Lithium.killBrowser
import lithium.Lithium.refresh
import lithium.Lithium.setDefaultTimeout
import lithium.Lithium.startChrome
import lithium.Lithium.text
import lithium.Lithium.waitUntil
import lithium.Lithium.write
import org.junit.Assert.assertEquals
import org.junit.Test
import org.openqa.selenium.By
import java.lang.Thread.sleep

class LithiumTest {

    @Test
    fun checkDefaultTimeout() {
        assertEquals(20L, getDefaultTimeout())
    }

    @Test
    fun modifyTimeoutAndCheck() {
        setDefaultTimeout(30L)
        assertEquals(30L, getDefaultTimeout())
    }

    @Test
    fun byId() {
        assertEquals(By.id("id"), getRightBy("#id"))
    }

    @Test
    fun byClassName() {
        assertEquals(By.className("class"), getRightBy(".class"))
    }

    @Test
    fun byName() {
        assertEquals(By.name("name"), getRightBy("@name"))
    }

    @Test
    fun byXpath() {
        assertEquals(By.xpath("//xpath"), getRightBy("//xpath"))
    }

    @Test
    fun byCssSelector() {
        assertEquals(By.cssSelector("selector"), getRightBy("selector"))
    }

    @Test
    fun startChromeBrowser() {
        startChrome(url = "https://google.com", headless = true)
        killBrowser()
    }

    @Test
    fun navigateTo() {
        startChrome(url = "https://material.angular.io/", headless = true)
        goTo("https://google.com")
        assertEquals("Check the title is Google", "Google", getTitle())
        killBrowser()
    }

    @Test
    fun checkTheTitle() {
        startChrome(url = "https://google.com", headless = true)
        assertEquals("Check the title is Google", "Google", getTitle())
        killBrowser()
    }

    @Test
    fun textShouldReturnWebElement() {
        startChrome(url = "https://material.angular.io/", headless = true)
        val getStartedElement = text(text = "Get started")
        waitUntil { getStartedElement.exists() }
        waitUntil { getStartedElement.isClickable() }
        click(getStartedElement)
        waitUntil { text("Getting started").exists() }
        killBrowser()
    }

    @Test
    fun textInsideSpecificElementShouldReturnWebElement() {
        startChrome(url = "https://material.angular.io/", headless = true)
        val getStartedElement = text(text = "Get started", element = "span")
        waitUntil { getStartedElement.exists() }
        waitUntil { getStartedElement.isClickable() }
        click(getStartedElement)
        waitUntil { text("Getting started", "h1").exists() }
        killBrowser()
    }

    @Test
    fun searchBabySharkYouTube() {
        startChrome(url = "https://www.youtube.com/", headless = true)
        isPageLoaded()
        write(el("#search"), "Baby shark")
        click(el("#search-icon-legacy"))
        sleep(2000)
        assertEquals("Check the title is Baby shark - YouTube", "Baby shark - YouTube", getTitle())
        waitUntil{text("Baby Shark Dance | Sing and Dance! | Animal Songs | PINKFONG Songs for Children").exists()}
        click(text("Baby Shark Dance | Sing and Dance! | Animal Songs | PINKFONG Songs for Children"))
        waitUntil{text("Baby Shark Dance | Sing and Dance! | Animal Songs | PINKFONG Songs for Children").exists()}
        killBrowser()
    }

    @Test
    fun refreshingPage() {
        startChrome(url = "https://google.com", headless = true)
        refresh()
        killBrowser()
    }
}
package lithium.exception

import java.lang.Exception

class NoSuchDriverInstantiatedException(message: String) : Exception(message)
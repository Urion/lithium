package lithium

import lithium.exception.NoSuchDriverInstantiatedException
import org.awaitility.Awaitility.await
import org.openqa.selenium.By
import org.openqa.selenium.Keys
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.firefox.FirefoxOptions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.Select
import java.util.concurrent.TimeUnit

object Lithium {
    // path to driver
    private var driverPath = ""

    private var timeout = 20L

    private lateinit var chromeDriver: ChromeDriver
    private lateinit var firefoxDriver: FirefoxDriver

    fun setDefaultTimeout(timeout: Long) {
        this.timeout = timeout
    }

    fun getDefaultTimeout(): Long = timeout

    fun startChrome(url: String = "", headless: Boolean = false, options: ChromeOptions = ChromeOptions()): WebDriver {
        if (driverPath != "")
            System.setProperty("webdriver.chrome.driver", driverPath)
        if (headless) {
            options.addArguments("--headless")
        }
        chromeDriver = ChromeDriver(options)
        chromeDriver.manage().window().maximize()
        if (url != "")
            chromeDriver.get(url)
        return chromeDriver
    }

    fun startFirefox(url: String = "", headless: Boolean = false, options: FirefoxOptions = FirefoxOptions()): WebDriver {
        if (driverPath != "")
            System.setProperty("webdriver.firefox.driver", driverPath)
        if (headless) {
            options.addArguments("--headless")
        }
        firefoxDriver = FirefoxDriver(options)
        firefoxDriver.manage().window().maximize()
        if (url != "")
            firefoxDriver.get(url)

        return firefoxDriver
    }

    fun setDriver(pathToDriver: String) {
        driverPath = pathToDriver
    }

    fun getDriver(): WebDriver {
        return when {
            chromeDriver.sessionId != null -> chromeDriver
            firefoxDriver.sessionId != null -> firefoxDriver
            else -> throw NoSuchDriverInstantiatedException("No one driver instantiated")
        }
    }

    fun goTo(url: String) {
        getDriver().navigate().to(url)
    }

    fun click(element: WebElement) {
        element.click()
    }

    fun waitUntil(f: () -> Boolean) {
        await()
            .atMost(timeout, TimeUnit.SECONDS)
            .until {
                f()
            }
    }

    fun write(element: WebElement, text: String) {
        element.sendKeys(text)
    }

    fun write(text: String) {
        val action = Actions(getDriver())
        action.sendKeys(text)
    }

    fun WebElement.exists() = this.isDisplayed

    fun WebElement.isClickable() = this.isDisplayed && this.isEnabled

    fun el(element: String): WebElement {
        return getDriver().findElement(getRightBy(element))
    }

    /**
     * this method find in the DOM of the page the specific element and return the element that contains it
     * @param text the text to find in the DOM
     * @param element it could be an HTML tag. For example can pass "a", "div", "span" etc. Default is all tags "*"
     * @return the WebElement that contains the text
     */
    fun text(text: String, element: String = "*"): WebElement {
        return getDriver().findElement(getRightBy("//$element[text()='$text']"))
    }

    fun refresh() {
        getDriver().navigate().refresh()
    }

    fun killBrowser() {
        getDriver().close()
    }

    fun press(key: Keys) {
        when {
            chromeDriver.sessionId != null -> {
                chromeDriver.keyboard.pressKey(key)
            }
            firefoxDriver.sessionId != null -> {
                firefoxDriver.keyboard.pressKey(key)
            }
            else -> throw NoSuchDriverInstantiatedException("No one driver instantiated")
        }
    }

    fun getTitle(): String {
        return getDriver().title
    }

    fun hover(element: WebElement) {
        val action = Actions(getDriver())
        action.moveToElement(element)
    }

    fun hover(element: WebElement, offsetX: Int, offsetY: Int) {
        val action = Actions(getDriver())
        action.moveToElement(element, offsetX, offsetY)
    }

    fun doubleClick(element: WebElement) {
        val action = Actions(getDriver())
        action.doubleClick(element)
    }

    fun doubleClick() {
        val action = Actions(getDriver())
        action.doubleClick()
    }

    fun rightClick(element: WebElement) {
        val action = Actions(getDriver())
        action.contextClick(element)
    }

    fun rightClick() {
        val action = Actions(getDriver())
        action.contextClick()
    }

    fun selectByValue(element: WebElement, value: String) {
        Select(element).selectByValue(value)
    }

    fun select(element: WebElement, value: String) {
        Select(element).selectByVisibleText(value)
    }

    fun select(element: WebElement, index: Int) {
        Select(element).selectByIndex(index)
    }

    fun isPageLoaded(timeout: Long = 10) {
        getDriver().manage().timeouts().pageLoadTimeout(timeout, TimeUnit.SECONDS);
    }

    /**
     * Used to get the right selenium By type
     * # is used to represent the id
     * . is used to represent the className
     * @ is used to represent the name
     * // is used to represent the xpath
     * otherwise, if nothing precede the element string cssSelector is used
     * @param element string that represent the get element
     * @return the correct one selenium By type
     */
    fun getRightBy(element: String): By {
        return when {
            element[0] == '#' -> By.id(element.substring(1, element.length))
            element[0] == '.' -> By.className(element.substring(1, element.length))
            element[0] == '@' -> By.name(element.substring(1, element.length))
            element[0] == '/' && element[1] == '/' -> By.xpath(element)
            else -> By.cssSelector(element)
        }
    }
}